\documentclass[a4paper]{easychair}
%%% metadata
\title{Weak distributive laws between powerspaces over stably compact spaces}
\author{Quentin Aristote}
\institute{
  Université Paris Cité, CNRS, Inria, IRIF, F-75013, Paris, France \\
  \email{quentin.aristote@irif.fr}
}
\authorrunning{Quentin Aristote}
\titlerunning{Weak distributive laws between powerdomain monads over stably
  compact spaces}

%%% packages
\usepackage{amsmath, amssymb} \usepackage{tikz-cd}
\usetikzlibrary{decorations.markings}
\tikzset{kleisli/.style={postaction={decorate, decoration={markings, mark=at
        position 0.5 with {\draw circle[radius=1.5pt];}}}}}
\usepackage[backend=biber, style=numeric, maxnames=2, firstinits=true,
url=false, doi=false, isbn=false]{biblatex}
\addbibresource{biblatex.bib}
\usepackage{fullpage}

%%% commands
\newcommand{\Set}{\mathbf{Set}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\CC}{\mathfrak{C}}
\newcommand{\D}{\mathcal{D}}
\renewcommand{\H}{\mathcal{H}}
\newcommand{\KK}{\mathfrak{K}}
\renewcommand{\P}{\mathcal{P}}
\newcommand{\Q}{\mathcal{Q}}
\newcommand{\R}{\mathcal{R}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\UU}{\mathfrak{U}}
\newcommand{\V}{\mathcal{V}}

\begin{document}

\maketitle

In the study of programming languages, effects may be modeled using monads. For
instance, the powerset monad $\P$ on the category $\Set$ models non-determinism:
a non-deterministic function from a set $X$ to a set $Y$ can be seen as a
(deterministic) function $X \rightarrow \P Y$. Similarly, the distribution monad
$\D$ and the corresponding functions $X \rightarrow \D Y$ model probabilistic
non-determinism: the monad $\D$ sends a set $X$ to the set of finitely supported
probability distributions on $X$. Ordinary and probabilistic non-determinism may
be combined in $\Set$ using \emph{weak distributive laws}, and in this work we
study how to combine these effects in a topological setting: we construct weak
distributive laws between powerspace monads over the category of stably compact
spaces, with the hope that weak distributive laws involving probabilistic
powerspace monads may also be constructed in future work.

Given two monads $\S$ and $\T$ on a category $\C$ corresponding to two effects,
the combination of these effects may be modeled by composing the monads $\S$ and
$\T$. This can be done by finding a \emph{distributive law} between the two
monads, i.e. a natural transformation $\T \S \Rightarrow \S \T$ satisfying
certain axioms. Such a distributive law yields a new monad $\S \circ \T$ on $C$
whose underlying functor is the composite of the functors underlying $\S$ and
$\T$.

But such distributive laws may not necessarily be unique and may not exist. In
particular, there is no distributive law $\P \P \Rightarrow \P \P$ nor $\D \P
\Rightarrow \P \D$ for combining two layers of non-determinism or
non-determinism with probabilistic non-determinisim
\cite{klinIteratedCovariantPowerset2018,dahlqvistCompositionalSemanticsNew2018}.
Still, there are natural transformations $\P \P \Rightarrow \P \P$ and $\D \P
\Rightarrow \P \D$ that satisfy all but one of the axioms required for them to
be distributive laws
\cite{garnerVietorisMonadWeak2020,goyCombiningProbabilisticNondeterministic2020}:
these natural transformations are called \emph{weak distributive laws}. For
instance, the weak distributive law $\lambda^{\P / \P} : \P \P \Rightarrow \P
\P$ is given for $\UU \in \P \P X$ by
\begin{equation}
  \label{eq:distr-law-powerset-powerset}
  \lambda^{\P /
    \P}_X(\UU) = \left\{ V \in \P X \mid V \subseteq \bigcup \UU
  \wedge \forall U \in \UU, U \cap V \neq \varnothing \right\}
\end{equation}
Again, weak distributive laws $\T \S \Rightarrow \S \T$ yield a composite monad,
but its underlying functor need not be the composite of the functors underlying
$\S$ and $\T$ anymore.

The powerset monad has a topological analogue, the Vietoris monad $\V$ on the
category of compact Hausdorff spaces (it sends a compact Hausdorff space to the
compact Hausdorff space of its closed subsets, equipped with the Vietoris
topology). It was shown recently that the weak distributive law $\lambda^{\P /
  \P}$ also has a topological analogue $\lambda^{\V / \V} : \V \V \Rightarrow \V
\V$ \cite{goyPowersetLikeMonadsWeakly2021}, given for $\CC \in \V \V X$ by
\begin{equation}
  \label{eq:distr-law-vietoris-vietoris}
  \lambda^{\V / \V}_X(\CC) = \left\{ K \in \V X \mid K \subseteq \bigcup \CC \wedge \forall C \in \CC, K \cap C \neq \varnothing \right\}
\end{equation}
(notice the resemblance with (\ref{eq:distr-law-powerset-powerset})).

What about a topological analogue of the weak distributive law $\D \P
\Rightarrow \P \D$ ? Goy conjectures in \cite{goyCompositionalityMonadsWeak2021}
that the strategy for constructing $\lambda^{\V / \V}$ can be adapted to
construct a weak distributive law $\R \V \Rightarrow \V \R$, where $\R$ is the
Radon monad -- a topological analogue of the distribution monad. But we show
that this conjecture does not hold, the main problem being that the category of
free algebras of the Vietoris monad is a strict subcategory of that of closed
relations between compact Hausdorff spaces.

At this point there are several directions to go towards for trying to get a
topological analogue of the weak distributive law $\D \P \Rightarrow \P \D$.
Here we choose to study weak distributive laws between powerspace monads in the
category of stably compact spaces instead \cite{lawsonStablyCompactSpaces2011},
as in this category the closed relations match exactly the free algebras of a
powerspace monad \cite{jungStablyCompactSpaces2001} (unlike for compact
Hausdorff spaces).

An important property of a stably compact space $(X,\tau)$ is that it comes with
its de Groot dual: $X$ can also be equipped with the topology $\tau^d$ whose
open sets are the compact saturated subsets of $(X,\tau)$. A continuous map $f :
X \rightarrow Y$ that is also continuous for the dual topologies is then said to
be \emph{proper}. There are then two notions of powerspaces, dual to one
another: the Smyth powerspace of a stably compact space $X$ is the space $\Q X$
of its compact saturated subsets equipped with the upper Vietoris topology,
while its Hoare powerspace is the space $\H X$ of its closed subsets equipped
with the lower Vietoris topology. It is well-known that $\Q X^d = (\H X)^d$: we
also show that the two monads themselves are de Groot dual of another, so that
their unit and multiplicative laws are proper maps.

We then construct a weak distributive law $\lambda^{\Q / \Q} : \Q \Q \Rightarrow
\Q \Q$ and a (strict) distributive law $\lambda^{\Q / \H} : \Q \H \Rightarrow \H
Q$, and dually. The weak distributive law $\lambda^{\Q / \Q}$ is obtained using
a known general construction starting from the identity monad morphism $\H
\Rightarrow \H$, and is given for $\KK \in \Q \Q X$ by
\begin{equation}
  \label{eq:distr-law-smyth-smyth}
  \lambda^{\Q / \Q}_X(\KK) = \left\{ K \in \Q X \mid K \subseteq \bigcup \KK \right\}
\end{equation}
The law $\lambda^{\Q / \H}$ is shown to be distributive by hand. It is given for
$\KK \in \Q \H X$ by
\begin{equation}
  \label{eq:distr-law-smyth-hoare}
  \lambda^{\Q / \H}_X(\KK) = \left\{ K \in \Q X \mid \forall C \in \KK, K \cap C \neq \varnothing \right\}
\end{equation}
and it is actually an isomorphism of monads $\Q \H \cong \H \Q$ with inverse the
dual distributive law $\H \Q \Rightarrow \Q \H$, so that $(\Q \H X)^d = \H \Q
X^d \cong \Q \H X^d$ and $\Q \H$ is self-dual.

Notice how, morally, combining (\ref{eq:distr-law-smyth-smyth}) and
(\ref{eq:distr-law-smyth-hoare}) would give back
(\ref{eq:distr-law-vietoris-vietoris}). In ongoing work, the author is studying
how the weak distributive law for the Vietoris monad can be generalized to a
weak distributive law for the Plotkin powerspace monad. While this law could
most likely be constructed by hand, the Plotkin powerspace monad arises
naturally as a combination of the Hoare and Smyth powerspaces, and there is hope
that making this combination categorical would yield the target weak
distributive law.

In further work the author plans to extend the combination of probabilistic
powerspaces over stably compact spaces with the Hoare and Smyth powerspaces
\cite{goubault-larrecqContinuousCapacitiesContinuous2007,goubault-larrecqGrootDualityModels2010}
to the monadic setting, again by constructing weak distributive laws.

\printbibliography

\end{document}
