{inputs, ...}: {
  imports = [inputs.my-nixpkgs.devenvModules.personal];

  languages = {
    nix.enable = true;
    texlive = {
      enable = true;
      packages = tl: {inherit (tl) scheme-full;};
      latexmk = {
        enable = true;
        extraConfig = ''
          $ENV{'TEXINPUTS'} = './easychair//:' . $ENV{'TEXINPUTS'}
        '';
      };
    };
  };
}
